import './App.css';
import { ProfileCompleteness } from './stories/ProfileCompletness/profileCompleteness.stories';

function App() {
  return (
    <div className="App">
      <ProfileCompleteness />
    </div>
  );
}

export default App;
