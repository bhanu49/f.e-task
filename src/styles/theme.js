const { createMuiTheme } = require('@material-ui/core');

export const VIOLET = '#502274';
const BLACK1 = '#555656';
const BLACK2 = '#878989';
const ORANGE = '#ED8838';

const theme = createMuiTheme({
  typography: {
    fontFamily: ['"OpenSans-Bold"', '"OpenSans-Regular"'].join(','),
    breakpoints: {
      keys: ['xs', 'sm', 'md', 'lg', 'xl'],
      values: {
        xs: 0,
        sm: 600,
        md: 960,
        lg: 1280,
        xl: 1920,
      },
    },
    h3: {
      color: BLACK1,
      fontFamily: '"OpenSans-Bold"',
      fontSize: 18,
    },
    body1: {
      fontSize: 15,
      fontFamily: '"OpenSans-Regular"',
      color: BLACK1,
    },
    body2: {
      fontSize: 15,
      fontFamily: '"OpenSans-Regular"',
      color: VIOLET,
    },
  },
  spacing: 8,
  palette: {
    primary: {
      main: VIOLET,
    },
  },
  overrides: {
    MuiButton: {},
  },
});

export default theme;
