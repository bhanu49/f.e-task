import React from 'react';
import Panel from '../Panel';
import {
  Box,
  Grid,
  Hidden,
  makeStyles,
  Typography,
  useMediaQuery,
} from '@material-ui/core';
import { Contained, Outlined } from '../AddButton/addButton.stories';
import { useTranslation } from 'react-i18next';
import { Indicator } from '../ProgressIndicator/progressIndicator.stories';

export default {
  title: 'profile completeness',
};

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(5),
    '& .MuiTypography-root': {
      padding: '4px 14px',
    },
    [theme.breakpoints.down('sm')]: {
      '& .MuiTypography-body1': {
        fontSize: 12,
      },
      '& .MuiTypography-body2': {
        fontSize: 12,
      },
      '& .MuiTypography-h3': {
        fontSize: 14,
      },
    },
  },
  wrap: {
    margin: theme.spacing(1.8),
  },
  header: {
    marginBottom: theme.spacing(2),
  },
}));

/**
 * responsive panel for profile completeness (uses Material UI Grid system)
 * @returns {JSX.Element}
 * @constructor
 */
export const ProfileCompleteness = () => {
  const classes = useStyles();
  const { t } = useTranslation(['common']);
  const matches = useMediaQuery((theme) => theme.breakpoints.up('sm'));

  return (
    <>
      <Panel classname={classes.root}>
        <Grid container spacing={3}>
          <Grid item md={7}>
            <Typography variant={'h3'} className={classes.header}>
              {t('common:completeness.main')}
            </Typography>

            <Hidden smUp>
              <Grid container justify={'center'} alignItems={'center'}>
                <Indicator
                  fontSize={'25px'}
                  percentage={78}
                  primaryColor={['#dd6343', '#e28a48']}
                  secondaryColor="#f7f7f7"
                  strokeWidth={16}
                  fontColor="black"
                  width={170}
                />
              </Grid>
            </Hidden>

            <Typography variant={'body1'} align={!matches ? 'center' : 'left'}>
              {t('common:completeness.text')}
            </Typography>
          </Grid>
          <Grid item md={5}>
            <Grid container>
              <Grid
                item
                md={6}
                container
                direction={'column'}
                justify="space-between"
              >
                <Box className={classes.wrap} display="flex">
                  <Outlined variant={'outlined'} color={'primary'} />
                  <Typography variant={'body2'}>
                    {t('common:yourCareer')}
                  </Typography>
                </Box>
                <Box className={classes.wrap} display="flex">
                  <Contained variant={'outlined'} color={'primary'} />
                  <Typography variant={'body2'}>
                    {t('common:skills')}
                  </Typography>
                </Box>
                <Box className={classes.wrap} display="flex">
                  <Contained variant={'outlined'} color={'primary'} />
                  <Typography variant={'body2'}>
                    {t('common:experience')}
                  </Typography>
                </Box>
              </Grid>

              <Grid
                item
                md={6}
                container
                direction="row"
                justify="center"
                alignItems={'center'}
              >
                <Hidden smDown>
                  <Indicator
                    fontSize={'30px'}
                    percentage={78}
                    primaryColor={['#dd6343', '#e28a48']}
                    secondaryColor="#f7f7f7"
                    strokeWidth={16}
                    fontColor="black"
                    width={200}
                  />
                </Hidden>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Panel>
    </>
  );
};
