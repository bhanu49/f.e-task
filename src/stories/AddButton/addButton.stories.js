import React, { useState } from 'react';
import { Button, makeStyles } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { VIOLET } from '../../styles/theme';
import clsx from 'clsx';

export default {
  title: 'Button',
  component: Button,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const useStyles = makeStyles((theme) => ({
  button: {
    padding: 14,
    minWidth: 0,
    width: 20,
    height: 20,
    borderRadius: 5,
    fontSize: 16,
    '&:hover': {
      backgroundColor: VIOLET,
      color: '#FFF',
    },
  },
  active: {
    backgroundColor: VIOLET,
    color: '#FFF',
  },
  [theme.breakpoints.down('sm')]: {
    button: {
      width: 15,
      height: 15,
      padding: 10,
      '& .MuiSvgIcon-root': {
        fontSize: 15,
      },
    },
  },
}));

const Template = (args) => {
  const classes = useStyles();
  const [active, setActive] = useState(false);

  /**
   * returns a custom button which toggles active status on click
   */
  return (
    <Button
      {...args}
      className={clsx(classes.button, active ? classes.active : '')}
      onClick={() => setActive(!active)}
    >
      <AddIcon />
    </Button>
  );
};

export const Outlined = Template.bind({});
Outlined.args = {
  variant: 'outlined',
  color: 'primary',
};

export const Contained = Template.bind({});
Contained.args = {
  variant: 'contained',
  color: 'primary',
};
