import React from 'react';
import { CircleProgress } from 'react-gradient-progress';

export default {
  title: 'Indicator',
  component: CircleProgress,
};

const Template = (args) => <CircleProgress {...args} />;

export const Indicator = Template.bind({});
Indicator.args = {
  percentage: 78,
  strokeWidth: 16,
  fontSize: '30px',
  primaryColor: ['#dd6343', '#e28a48'],
  secondaryColor: '#f7f7f7',
  fontColor: 'black',
  width: 200,
};
