import React from 'react';
import Panel from '.';

export default {
  title: 'INUI/Panel',
  component: Panel,
};

const Text = (props) => <div {...props} />;

export const Default = () => <Panel style={{ marginTop: 100 }} />;
