import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from '../src/styles/theme';
import { CssBaseline } from '@material-ui/core';
import { I18nextProvider } from 'react-i18next';
import i18n from '../src/i18n';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
};

export const decorators = [
  (Story) => (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Story />
    </ThemeProvider>
  ),
  (Story) => (
    <I18nextProvider i18n={i18n}>
      <Story />
    </I18nextProvider>
  ),
];
